import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI



WebUI.openBrowser('https://gen5-qc.asuransiastra.com/retail')

WebUI.maximizeWindow()

String password = 'ITG@nt1P455QC'

//cek spk offline pay to tertanggung
String claimno = UI.getValueDatabase("172.16.94.74", "AAB", "Select top 1 * from dbo.claim where settled_status='1' and is_online_workshop ='1' order by entrydt desc", "claim_no")
//String claimno = UI.getValueDatabase("172.16.94.74", "AAB", "SELECT top 1 * FROM claim order by EntryDt desc","claim_no")
println(claimno)
WebUI.callTestCase(findTestCase('Screen/Login/Login Claim Tracking'), 
	[('password') : password], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Screen/Claim Tracking/Claim Tracking detail'), [('claimno') : claimno], FailureHandling.STOP_ON_FAILURE)

WebUI.closeBrowser()

