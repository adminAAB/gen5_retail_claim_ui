<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CECKBOX_PANEL</name>
   <tag></tag>
   <elementGuidId>78983849-27a4-46d8-876a-36bd9fb25885</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;P3S0-0&quot;]/div[1]/div[1]/a2is-datatable/div[2]/div/table/tbody//tr[${row}]//*[@type=&quot;checkbox&quot;][count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;P3S0-0&quot;]/div[1]/div[1]/a2is-datatable/div[2]/div/table/tbody//tr[${row}]//*[@type=&quot;checkbox&quot;]</value>
   </webElementProperties>
</WebElementEntity>
