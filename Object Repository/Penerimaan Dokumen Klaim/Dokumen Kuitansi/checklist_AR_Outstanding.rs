<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>checklist_AR_Outstanding</name>
   <tag></tag>
   <elementGuidId>6b9df607-c031-448f-97e0-708eb036c1cb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;CDRP-0&quot;]//span[text()='AR Outstanding']/parent::label/following-sibling::div//span[text()='Adjustment']/parent::div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;CDRP-0&quot;]//span[text()='AR Outstanding']/parent::label/following-sibling::div//span[text()='Adjustment']/parent::div/input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/FRAME</value>
   </webElementProperties>
</WebElementEntity>
