import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testdata.DBData

import com.keyword.GEN5
import com.keyword.UI

/*
String offline = UI.getValueDatabase("172.16.94.74","AAB","SELECT TOP 1 * FROM claim c WHERE workshop_code='00453417' and user_id='BTE' and not EXISTS (SELECT * FROM  claim_reception_spk crs WHERE c.Claim_No=crs.SPK_No)  AND claimable_status <>'2'", "claim_no")

println (offline)
String online = UI.getValueDatabase("172.16.94.74", "AAB", "SELECT TOP 1 * FROM claim c INNER JOIN cps..trx_spk ts on ts.spk_no=c.Claim_No INNER JOIN dbo.Claim_Reception_SPK crs on ts.SPK_No=crs.spk_no INNER JOIN dbo.Claim_Reception cr on cr.Reception_ID = crs.Reception_ID WHERE c.workshop_code='90332449'   AND ts.SPK_Status='05'  and cr.Reception_ID like '%PLCE%' and cr.RejectF=0 and c.claimable_status <>'2'", "claim_no")

println (online)
String AwalTLS = UI.getValueDatabase("172.16.94.74","AAB","select top 1 * from claim where type_loss_id='TLOSTL' and USER_id='fmw' order by entrydt desc", "claim_no")
println (AwalTLS)
String AwalTLA = UI.getValueDatabase("172.16.94.74","AAB","select top 1 * from claim where type_loss_id='TLOACC' and USER_id='fmw' order by entrydt desc", "claim_no")
println (AwalTLA)
String Akhir = UI.getValueDatabase("172.16.94.74","AAB","SELECT top 1 * FROM claim c inner join claim_reception_spk crs ON c.claim_no=crs.spk_no inner join claim_reception cr  on crs.reception_id = cr.Reception_ID where  type_loss_id in ('TLOSTL','TLOACC') AND workshop_code='' and cr.status='19' and cr.rejectF='0'", "claim_no")
println (Akhir)


ArrayList detail2 = UI.getOneRowDatabase("172.16.94.74", "AAB", "SELECT '1' as NO, mc.name, Master_No, c.claim_no, CASE WHEN c.isb2b=0 THEN 'No' when c.isb2b=1 then 'Yes' END isB2B  FROM claim c INNER JOIN dbo.Mapping_Master_Claim mmc ON mmc.Claim_No = c.Claim_No INNER JOIN  mst_customer mc on c.pay_to_code=mc.cust_id where c.claim_no='3190003840'")
println (detail2)

def spkstatus = findTestData("SPK status").getValue(10, 1)
def spkno = findTestData("SPK status").getValue(2, 1)
def masterspk = findTestData("SPK status").getValue(11, 1).trim()
println(spkstatus)
println(spkno)

DBData validasi = findTestData("validasi spk")
	validasi.query = validasi.query.replace('_spkno_',spkno)
	validasi.fetchedData = validasi.fetchData()
	
	def mspk = validasi.getValue(1, 1)
	def aging = validasi.getValue(2, 1)
	def description =  validasi.getValue(3, 1)
	def receipt_date = validasi.getValue(4, 1)
	def invoice_date = validasi.getValue(5, 1)
	def workshop_name = validasi.getValue(6, 1)
*/
/*String claimno = UI.getValueDatabase("172.16.94.74", "AAB", "SELECT TOP 1 * FROM claim c INNER JOIN cps..trx_spk ts on ts.spk_no=c.Claim_No WHERE c.workshop_code='90332449' and user_id='BTE'  AND ts.SPK_Status='03'  and c.claimable_status <>'2' and not EXISTS (SELECT * FROM  claim_reception_spk crs WHERE c.Claim_No=crs.SPK_No) order by c.entrydt desc",
	"claim_no")

println(claimno)

def mspk = UI.getValueDatabase("172.16.94.74", "AAB", "Select top 1 * from dbo.claim c inner join mapping_master_claim mmc on c.claim_no=mmc.claim_no where c.claim_no='3200000433'", "master_no")
println(mspk)*/
	
def spk = UI.getValueDatabase("172.16.94.74", "AAB", "SELECT TOP 1 * FROM claim WHERE is_online_workshop ='0' and claimable_status <>'2' and Type_Loss_Id in ('TLOACC') and not EXISTS (SELECT * FROM  claim_reception_spk crs WHERE Claim_No=crs.SPK_No) order by entrydt DESC",
	"claim_no")

println(spk)