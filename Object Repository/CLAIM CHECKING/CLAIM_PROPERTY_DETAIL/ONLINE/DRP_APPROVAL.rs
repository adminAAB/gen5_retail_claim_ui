<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DRP_APPROVAL</name>
   <tag></tag>
   <elementGuidId>0b503239-0db3-44a0-b266-08f18acb2206</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[not(contains(@style,&quot;none&quot;)) and @data-bind=&quot;visible: ($parent.active()==$index())&quot; and @style]//div[contains(@id,&quot;pageset&quot;)]//div[contains(@class,&quot;a2is-content-for-pageset&quot;)]//h3[text()=&quot;Claim Property Detail&quot;]/parent::*//span[text()='Approval']/parent::label/following-sibling::div[count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[not(contains(@style,&quot;none&quot;)) and @data-bind=&quot;visible: ($parent.active()==$index())&quot; and @style]//div[contains(@id,&quot;pageset&quot;)]//div[contains(@class,&quot;a2is-content-for-pageset&quot;)]//h3[text()=&quot;Claim Property Detail&quot;]/parent::*//span[text()='Approval']/parent::label/following-sibling::div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/FRAME</value>
   </webElementProperties>
</WebElementEntity>
