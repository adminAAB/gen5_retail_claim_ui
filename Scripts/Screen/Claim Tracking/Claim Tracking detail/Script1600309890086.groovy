import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testdata.DBData

import com.keyword.UI
import com.keyword.GEN5

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Claim Tracking/expand_claim'))

println ("sudah masuk ke halaman claim tracking")

WebUI.click(findTestObject('Object Repository/Claim Tracking/btn_claim tracking'))

GEN5.ProcessingCommand()

//mencari spk bengkel offline

//String SPKoffline = UI.getValueDatabase("172.16.94.74", "AAB", "Select * from dbo.claim where claim_no='"+claimno+"'", "Is_Online_Workshop")
String spkno = UI.getValueDatabase("172.16.94.74", "AAB", "SELECT top 1 * FROM claim where claim_no='"+claimno+"'","claim_no")
println (spkno)
//println(spkno)

GEN5.ProcessingCommand()

WebUI.setText(findTestObject('Object Repository/Claim Tracking/txt_spk'),claimno)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Claim Tracking/btn_search'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Claim Tracking/btn_list_spk'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Claim Tracking/btn_detail'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Claim Tracking/tab_action history'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Claim Tracking/tab_document'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Claim Tracking/tab_payment'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Claim Tracking/tab_photo'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Claim Tracking/tab_image doc'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Claim Tracking/tab_summary'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Claim Tracking/tab_history detail'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Claim Tracking/tab_claim history'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Claim Tracking/tab_policy summary'))

GEN5.ProcessingCommand()


