<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Sprint 31</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d67755ad-1bca-4043-86cf-deaf4c6b90fe</testSuiteGuid>
   <testCaseLink>
      <guid>5068279a-c219-4809-9679-7c59e2125781</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim NEW/Offline - Estimasi Partial Loss Accident Bengkel - Reject CCO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d57753a-76b9-4d11-b00c-024cd6bf9e28</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim NEW/Offline - Invoice - PARACC Tertanggung - Reject CCO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5c262aac-c19c-43e6-a611-b9f20d26a177</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim NEW/Offline - Invoice PARSTO Bengkel - Approve CCO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f818d7c6-b338-4f71-adf7-c82cd777d831</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/Claim NEW/Offline - Invoice PARACC Bengkel - Reject CCO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>60f4cf38-a23f-4be7-8164-2903d8ba73f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim NEW/Claim Offline - Bengkel - Approve CCO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c8a83a78-e53f-45ca-8aa9-36722bac441e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim NEW/Claim Offline - Bengkel - Reject CCO</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
