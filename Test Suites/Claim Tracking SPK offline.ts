<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Claim Tracking SPK offline</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>6749dd89-eb4d-4719-9c5d-3c076c082fed</testSuiteGuid>
   <testCaseLink>
      <guid>0fe7f792-0cad-4d71-abe8-3f82c72ee9fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Tracking/SPK offline/Tracking SPK offline - Pay to bengkel offline PARACC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d6a7cb86-1fd3-43f3-b72d-f63200ddca80</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Tracking/SPK offline/Tracking SPK offline - Pay to bengkel offline PARSTO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>da213bd4-baad-4ba7-b0a0-119f0a7a299a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Tracking/SPK offline/Tracking SPK offline - Pay to bengkel offline</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44000f44-380a-4815-a5bd-2f4fde23bdc5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Tracking/SPK offline/Tracking SPK offline - Pay to tertanggung TLOACC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a5f94338-7e8b-4a69-a7f1-39b6905f58fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Tracking/SPK offline/Tracking SPK offline - Pay to tertanggung TLOSTL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>339664b9-cd0c-48c7-935c-8649d2969f1e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Tracking/SPK offline/Tracking SPK offline - Pay to tertanggung</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4f23e4a5-5bec-4cf0-ba6e-b0915be747cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Tracking/SPK offline/Tracking SPK offline - Settle status 0</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d592864-3b10-4a0a-806a-7bd1eff01b4b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Tracking/SPK offline/Tracking SPK offline - Settle status 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0ae374b6-1420-4054-b031-1e85cb109ded</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/Claim Tracking/SPK offline/Tracking SPK offline</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
