import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI

//cari data claimno SPK Online status cps waiting checker approval pastikan belom ada di claim reception

String claimno = UI.getValueDatabase("172.16.94.74", "AAB", "SELECT TOP 1 * FROM claim a inner JOIN dbo.Dtl_Claim_Interest b ON a.Claim_No=b.Claim_No WHERE is_online_workshop ='0' and Pay_to_code = '' and claimable_status <>'2' and not EXISTS (SELECT * FROM  claim_reception_spk crs WHERE a.Claim_No=crs.SPK_No) order by a.entrydt DESC",
	"claim_no")

println(claimno)


//define semua variable yang ada di screen
//proses estimasi di GEN5 menggunakan user OMA dan SST yaitu checker dan approval
ArrayList proses = ['checking','approval']

int i 

String password = 'P@ssw0rd'

String tipelogin = 'a2is Retail - Konvensional'

String nomorrangka = UI.getValueDatabase("172.16.94.74", "AAB", "SELECT top 1 * FROM claim c INNER JOIN dtl_mv dm ON c.policy_id=dm.policy_id where c.claim_no='"+claimno+"'", "chasis_number")

String nilaiestimasi = '3000000'

String remark = 'remarks skenario reject checker estimasi'

String receivedate = '10/Mar/2020'

String invoicedate = '10/Mar/2020'

String nomorkuitansi = '1234567890'

String nilaikuitansi = '3000000'

String SAname = 'Farida Ambarwati'

String nomorBPKB = '1234567890'



for (i = 0; i < proses.size(); i++) {
	
	WebUI.openBrowser('https://gen5-qc.asuransiastra.com/retail')
	
	WebUI.maximizeWindow()
	
	WebUI.callTestCase(findTestCase('Screen/Login/Login'), 
		[('password') : password,
		('tipelogin') : tipelogin , 
		('claimno') : claimno,
		('proses') : proses[i] ],
		FailureHandling.STOP_ON_FAILURE)
		
	WebUI.callTestCase(findTestCase('Screen/Home/Home'), [('proses') : proses[i]], FailureHandling.STOP_ON_FAILURE)
	
	if (proses[i] == 'dokumen') {
		
		WebUI.callTestCase(findTestCase('Screen/Penerimaan Dokumen Klaim/Penerimaan Dokumen'), [('claimno') : claimno], FailureHandling.STOP_ON_FAILURE)
		
			WebUI.callTestCase(findTestCase('Screen/Penerimaan Dokumen Klaim/All dokumen'),
			[('claimno') : claimno,
				('nomorrangka') : nomorrangka,
				('nilaiestimasi') : nilaiestimasi,
				('nilaikuitansi') : nilaikuitansi,
				('receivedate') : receivedate,
				('invoicedate') : invoicedate,
				('SAname') : SAname,
				('nomorBPKB') : nomorBPKB,
				('remarks') : remark ], FailureHandling.STOP_ON_FAILURE)
			
		
	} else if (proses[i] == 'checking') {
	
	WebUI.callTestCase(findTestCase('Screen/claim/Claim Checking'),  [('claimno') : claimno , ('Approval') : 'Reject'], FailureHandling.STOP_ON_FAILURE)
	
	} else if (proses[i] == 'approval') {
		
		WebUI.callTestCase(findTestCase('Screen/Claim Checking/Claim Checking Detail'), [('claimno') : claimno , ('remark') : remark, ('approval') : 'Approve'],   FailureHandling.STOP_ON_FAILURE)
	}
	
}
