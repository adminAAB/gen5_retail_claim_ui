<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_CLAIM_HISTORY</name>
   <tag></tag>
   <elementGuidId>ba8768fb-8e97-4a9d-8759-58c52dc01664</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;pageset1-1&quot;]/div[1]/div/a2is-buttons/div/div/button[text()='Claim History']

//*[@id=&quot;pageset1-1&quot;]//a2is-buttons//div/div/button[text()='Claim History'][count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;pageset1-1&quot;]/div[1]/div/a2is-buttons/div/div/button[text()='Claim History']

//*[@id=&quot;pageset1-1&quot;]//a2is-buttons//div/div/button[text()='Claim History']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/FRAME</value>
   </webElementProperties>
</WebElementEntity>
