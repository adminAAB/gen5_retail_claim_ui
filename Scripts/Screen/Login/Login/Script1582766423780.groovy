import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.testdata.DBData

import com.keyword.UI
import com.keyword.GEN5

String status = UI.getValueDatabase("172.16.94.74", "AAB", "SELECT TOP 1 Status, * FROM claim c INNER JOIN claim_reception_spk crs on c.claim_no=crs.spk_no inner join claim_reception cr on cr.reception_id=crs.reception_id where c.claim_no='"+claimno+"' and cr.rejectf='0'" ,
	"status")
println(status)

//status cps..trx_spk
String statusbengkel =  UI.getValueDatabase("172.16.94.74", "AAB", "SELECT TOP 1 * FROM claim c INNER JOIN cps..trx_spk ts on ts.spk_no=c.Claim_No WHERE c.claim_no='"+claimno+"'",
	"spk_status")

//WebUI.setText(findTestObject('Object Repository/LOGIN/USER_ID'), userid)

//login approval 14 BOD sesuai dengan status claim
if (status == '14' && proses == 'approval'){
	
	WebUI.setText(findTestObject('Object Repository/LOGIN/USER_ID'), 'HYO')
//approval 13 VP	
}else if(status == '13' && proses == 'approval'){

	WebUI.setText(findTestObject('Object Repository/LOGIN/USER_ID'), 'MWU')
//approval status Manager	
}else if (status == '12' && proses == 'approval'){

	WebUI.setText(findTestObject('Object Repository/LOGIN/USER_ID'), 'WHI')
//approval section Head	
}else if (status == '11' && proses == 'approval'){

	WebUI.setText(findTestObject('Object Repository/LOGIN/USER_ID'), 'HRE')
//approval CCO	
}else if (status == '10' && proses == 'approval'){

	WebUI.setText(findTestObject('Object Repository/LOGIN/USER_ID'), 'SST')
} else if ((status == '1' || status == '2' || statusbengkel == '03' ) && proses == 'checking') {

	WebUI.setText(findTestObject('Object Repository/LOGIN/USER_ID'), 'OMA') //FMW - tambah kondisi untuk login OMA
	
} else {

	WebUI.setText(findTestObject('Object Repository/LOGIN/USER_ID'), 'EDN') //FMW - tambah kondisi untuk login OMA

}
WebUI.setText(findTestObject('Object Repository/LOGIN/PASSWORD'), password)

WebUI.click(findTestObject('Object Repository/LOGIN/DRP_LOGIN_TIPE'))

WebUI.click(findTestObject('LOGIN/LST_LOGIN_TIPE - listlogin',[('listlogin'): tipelogin]))

WebUI.click(findTestObject('Object Repository/LOGIN/BTN_MASUK'))