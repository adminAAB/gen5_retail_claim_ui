import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI

//GEN5.ProcessingCommand()
WebUI.delay(5)

//Menentukan SPK pay to bengkel offline atau online
String SPKonline = UI.getValueDatabase("172.16.94.74", "AAB", "Select * from dbo.claim where claim_no='"+claimno+"'", "Is_Online_Workshop")

println (SPKonline)

//menentukan SPK partial loss atau total loss
String claimtype = UI.getValueDatabase("172.16.94.74", "AAB", "Select * from dbo.claim where claim_no='"+claimno+"'", "Type_Loss_Id")

println (claimtype)

//cek payto tertanggung
String payto = UI.getValueDatabase("172.16.94.74", "AAB", "Select * from dbo.claim where claim_no='"+claimno+"'", "workshop_code")
println(payto)

//cek status SPK (digunakan untuk total loss)
String status = UI.getValueDatabase("172.16.94.74", "AAB", "SELECT * FROM claim_reception cr INNER JOIN dbo.Claim_Reception_SPK crs on crs.Reception_ID = cr.Reception_ID where SPK_No='"+claimno+"' and cr.rejectF='0'",
	"Status")
println (status)

//menentukan B2B atau bukan
String isB2B = UI.getValueDatabase("172.16.94.74", "AAB", "Select * from dbo.claim where claim_no='"+claimno+"'", "isB2B")
println (isB2B)

//melihat checklist biaya derek ada di UI atau tidak
boolean derek = WebUI.waitForElementVisible(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Dokumen Kuitansi/checklist_Biaya_Derek'), 1, FailureHandling.OPTIONAL)
println(derek)

//melihat AR Outstanding ada di UI atau tidak
boolean AR = WebUI.waitForElementVisible(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Dokumen Kuitansi/checklist_AR_Outstanding'), 1, FailureHandling.OPTIONAL)
println(AR)

//GEN5.ProcessingCommand()

WebUI.delay(5)

if ((SPKonline == '1') || ((SPKonline == '0') && (status == '15'))) {
	
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Kuitansi/checklist_kuitansi_bengkel'))
	
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Kuitansi/checklist_Dokumen_SPK'))
	
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Kuitansi/checklist_Gesekan_rangka'))
	
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Kuitansi/checklist_BPK_Bengkel'))
	
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Kuitansi/checklist_Rincian_perBody'))
	
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Kuitansi/checklist_Faktur_Pajak'))
	
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Kuitansi/checklist_Biaya_Ekspedisi'))
	
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Kuitansi/checklist_KTP'))
	
	
	// jika SPK offline, user perlu input no rangka, nomor kuitansi, nilai kuitansi, invoice date
	// jika SPK online, user HANYA perlu input no rangka, nilai kuitansi
	// SPK selain B2B wajib isi SAname
	
	if (isB2B == '0' && SPKonline == '0') {
		WebUI.setText(findTestObject('Penerimaan Dokumen Klaim/Dokumen Kuitansi/SA_name'), SAname)
		
		WebUI.click(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Dokumen Kuitansi/button_invoice_date'))
		
		GEN5.DatePicker(invoicedate, findTestObject('Object Repository/Penerimaan Dokumen Klaim/Dokumen Kuitansi/choose_invoice_date'))
		
		WebUI.setText(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Dokumen Kuitansi/text_nomor_kuitansi'), nomorkuitansi)
		
	} else if (isB2B == '0' && SPKonline == '1') {
	
		WebUI.setText(findTestObject('Penerimaan Dokumen Klaim/Dokumen Kuitansi/SA_name'), SAname)
		
		/*WebUI.click(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Dokumen Kuitansi/button_invoice_date'))
		
		GEN5.DatePicker(invoicedate, findTestObject('Object Repository/Penerimaan Dokumen Klaim/Dokumen Kuitansi/choose_invoice_date'))
		
		WebUI.setText(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Dokumen Kuitansi/text_nomor_kuitansi'), nomorkuitansi)
		*/
	} 
	if (derek == true) {
	
		WebUI.click(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Dokumen Kuitansi/checklist_Biaya_Derek'))
	}
	if (AR == true){
		
		WebUI.click(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Dokumen Kuitansi/checklist_AR_Outstanding'))
	}
	
	WebUI.setText(findTestObject('Penerimaan Dokumen Klaim/Dokumen Kuitansi/text_no_rangka'), nomorrangka)
	
	WebUI.setText(findTestObject('Penerimaan Dokumen Klaim/Dokumen Kuitansi/text_nilai_kuitansi'), nilaikuitansi)
	
	WebUI.setText(findTestObject('Penerimaan Dokumen Klaim/Dokumen Kuitansi/text_remarks'), remarks)
	
} else if (SPKonline == '0' && payto != '' &&((claimtype == 'PARACC') || (claimtype == 'PARSTO'))) {

	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Estimasi/checklist_estimasi_bengkel'))

	WebUI.setText(findTestObject('Penerimaan Dokumen Klaim/Dokumen Estimasi/text_nilai_estimasi'), nilaiestimasi)

	WebUI.setText(findTestObject('Penerimaan Dokumen Klaim/Dokumen Estimasi/text_remarks'), remarks)
	
} else if (payto == '' && SPKonline =='0' && ((claimtype == 'PARACC') || (claimtype == 'PARSTO'))) {

	WebUI.click(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Dokumen Kuitansi Tertanggung/checklist_kuitansi_bengkel'))
	
	WebUI.click(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Dokumen Kuitansi Tertanggung/checklist_Dokumen_SPK'))
	
	WebUI.click(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Dokumen Kuitansi Tertanggung/checklist_Rincian_perBody'))
	
	WebUI.click(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Dokumen Kuitansi Tertanggung/checklist_tanda_terima_salvage'))
	
	WebUI.click(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Dokumen Kuitansi Tertanggung/checklist_form_bengkel_tertanggung'))
	
	WebUI.click(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Dokumen Kuitansi Tertanggung/checklist_adj_bengkel_tertanggung'))
	
	WebUI.setText(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Dokumen Kuitansi Tertanggung/text_nomor_kuitansi'), nilaikuitansi)
	
	WebUI.setText(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Dokumen Kuitansi Tertanggung/text_nilai_kuitansi'), nilaikuitansi)
	
	WebUI.setText(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Dokumen Kuitansi Tertanggung/text_remarks'), remarks)
	
	
} else if (status == '19' && claimtype == 'TLOACC' ) {

	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir/checklist_dokumen_SPC'))

	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir/checklist_BPKB_Asli'))

	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir/checklist_faktur_pembelian'))

	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir/checklist_blanko_kuitansi'))

	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir/checklist_abandonment'))

	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir/checklist_STNK_Asli'))

	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir/checklist_kunci_kontak'))
/*
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir/checklist_polis_asli'))

	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir/checklist_buku_KIR'))

	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir/checklist_Paneng'))
	*/
	WebUI.setText(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir/text_nomor_BPKB'), nomorBPKB)

	WebUI.setText(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir/text_remarks'), remarks)

} else if (status == '19' && claimtype == 'TLOSTL') {

	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir TLS/checklist_dokumen_SPC'))

	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir TLS/checklist_BPKB_Asli'))

	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir TLS/checklist_faktur_pembelian'))

	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir TLS/checklist_blanko_kuitansi'))

	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir TLS/checklist_abandonment'))

	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir TLS/checklist_STNK_Asli'))

	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir TLS/checklist_kunci_kontak'))

	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir TLS/checklist_polis_asli'))

	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir TLS/checklist_buku_KIR'))

	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir TLS/checklist_Paneng'))
	
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir TLS/checklist_cabut_blokir'))
	
	WebUI.setText(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir TLS/text_nomor_BPKB'), nomorBPKB)

	WebUI.setText(findTestObject('Penerimaan Dokumen Klaim/Dokumen Akhir TLS/text_remarks'), remarks)
	
} else if (claimtype == 'TLOACC')  {
	
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Awal-TLA/checklist_Dokumen_SPK'))
	
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Awal-TLA/checklist_kerugian'))
	
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Awal-TLA/checklist_form_interview'))
	
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Awal-TLA/checklist_fotokopi_KTP'))
	
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Awal-TLA/checklist_fotokopi_SIM'))
	
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Awal-TLA/checklist_fotokopi_STNK'))
	
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Awal-TLA/checklist_laporan_polisi'))
	
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Awal-TLA/checklist_estimasi'))
	
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Awal-TLA/checklist_laporan_surveyTLA'))
	
	WebUI.setText(findTestObject('Penerimaan Dokumen Klaim/Dokumen Awal-TLA/text_nilai_estimasi'), nilaiestimasi)
	
	WebUI.setText(findTestObject('Penerimaan Dokumen Klaim/Dokumen Awal-TLA/text_remarks'), remarks)
	
} else if (claimtype == 'TLOSTL') {
		
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Awal-TLS/checklist_Dokumen_SPK'))
		
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Awal-TLS/checklist_laporan_kerugian'))
		
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Awal-TLS/checklist_form_interview'))
		
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Awal-TLS/checklist_fotokopi_KTP'))
		
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Awal-TLS/checklist_fotokopi_SIM'))
		
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Awal-TLS/checklist_fotokopi_STNK'))
		
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Awal-TLS/checklist_laporan_polisi'))
		
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Awal-TLS/checklist_cek_TKP'))
		
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Awal-TLS/checklist_fotokopi_kaditserse'))
		
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Awal-TLS/checklist_kaditserse_asli'))
		
	WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Dokumen Awal-TLS/checklist_blokir_asli'))
		
	WebUI.setText(findTestObject('Penerimaan Dokumen Klaim/Dokumen Awal-TLS/text_remarks'), remarks)
		
	}

//Proses simpan dokumen (karena xpath berlaku untuk semua jenis dokumen)

WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Proses Simpan/button_Simpan'))

/*WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Proses Simpan/button_tanggalterima'))

GEN5.DatePicker(receivedate, findTestObject('Object Repository/Penerimaan Dokumen Klaim/Proses Simpan/choose_date'))
*/
WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Proses Simpan/button_Simpan_Tanggalterima'))

WebUI.delay(3)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Proses Simpan/button_Tutup'))

WebUI.delay(3)

/*GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/Logout/DRP_NAMA'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Logout/BTN_EXIT'))

WebUI.delay(3)*/




