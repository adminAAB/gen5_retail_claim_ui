import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.builtin.CloseBrowserKeyword

import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testdata.DBData

import com.keyword.UI
import com.keyword.GEN5

WebUI.verifyElementText(findTestObject('Object Repository/CLAIM CHECKING/SEARCH/LBL_TASKLIST'),'Task List')

GEN5.ProcessingCommand()

println ("sudah masuk ke halaman claim checking")

def isonline = UI.getValueDatabase("172.16.94.74", "AAB", "Select * from dbo.claim where claim_no='"+claimno+"'", "Is_Online_Workshop")

//dipakai untuk kondisi validasi pay to ke tertanggung atau ke bengkel online/offline
def paytocode =  UI.getValueDatabase("172.16.94.74", "AAB", "Select * from dbo.claim where claim_no='"+claimno+"'", "pay_to_code")
println(paytocode)

WebUI.setText(findTestObject('Object Repository/CLAIM CHECKING/SEARCH/TXT_SPK_NO'),claimno)

WebUI.click(findTestObject('Object Repository/CLAIM CHECKING/SEARCH/BTN_SEARCH'))

GEN5.ProcessingCommand()

WebUI.delay(3)

if (paytocode == 'null' ){
/* COMPARE SPK DI HALAMAN SEARCH TASKLIST CLAIM CHECKING */ 

	//Harus replace data variabel di data files
	DBData tasklist = findTestData('Tasklist search spk')
	tasklist.query = tasklist.query.replace("_claimno_",claimno)
	tasklist.fetchedData = tasklist.fetchData()
	
	//def kolom - kolom yang ada di tasklist yang akan dicompare dengan database 
	def masterspk = tasklist.getValue(1, 1)
	def aging	=  tasklist.getValue(2, 1)
	def description = tasklist.getValue(3, 1)
	def receiptdt = tasklist.getValue(4, 1)
	def invoicedt =  tasklist.getValue(5, 1)
	def workshopname = tasklist.getValue(6, 1)
	//def isonline = tasklist.getValue(7, 1)

	//list 
	ArrayList comparespk = [masterspk,aging,description,receiptdt,invoicedt,workshopname]
	
	GEN5.CompareRowsValue(findTestObject('Object Repository/CLAIM CHECKING/SEARCH/TBL_DATA_SEARCH'), "No", "1", comparespk)
}

else {
}
	
WebUI.click(findTestObject('Object Repository/CLAIM CHECKING/SEARCH/TBL_TOP_1'))
	
WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/CLAIM CHECKING/SEARCH/BTN_NEXT'))

WebUI.delay(5)

WebUI.verifyElementText(findTestObject('Object Repository/CLAIM CHECKING/DETAIL/TAB_SPK/LBL_SPK'), 'SPK List')

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/CLAIM CHECKING/DETAIL/TAB - TAB_CLAIM_CHECKING - tabclaimchecking',[('tabclaimchecking'): 'Photo' ]))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/CLAIM CHECKING/DETAIL/TAB - TAB_CLAIM_CHECKING - tabclaimchecking',[('tabclaimchecking'): 'Claim Info' ]))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/CLAIM CHECKING/DETAIL/TAB - TAB_CLAIM_CHECKING - tabclaimchecking',[('tabclaimchecking'): 'Document' ]))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/CLAIM CHECKING/DETAIL/BTN_NEXT'))

WebUI.delay(3)

// MASUK KE HALAMAN CLAIM PROPERTY DETAIL

WebUI.verifyElementText(findTestObject('CLAIM CHECKING/CLAIM_PROPERTY_DETAIL/OFFLINE/LBL_CLAIM_PROPERTY_OFF'), 'Claim Property Detail')
	
WebUI.click(findTestObject('Object Repository/CLAIM CHECKING/CLAIM_PROPERTY_DETAIL/OFFLINE/DRP_APPROVAL'))
	
	if ( approval == 'Approve'){
		
		WebUI.click(findTestObject('Object Repository/CLAIM CHECKING/CLAIM_PROPERTY_DETAIL/OFFLINE/LST_APPROVAL - listapprovaloffline',[('listapprovaloffline'): approval]))
		
		WebUI.delay(3)
		
		//di kondisi karena path yang offline beda dengan online
		if (isonline == '1') {
			//variable di screen untuk remarks, di tulis di scenario
			WebUI.setText(findTestObject('Object Repository/CLAIM CHECKING/CLAIM_PROPERTY_DETAIL/ONLINE/TXT_REMARK'),remark)
			println(remark)	
			
		}else if (isonline == '0' ){
		
			if (paytocode == 'null' || paytocode == '') {
			WebUI.setText(findTestObject('Object Repository/CLAIM CHECKING/CLAIM_PROPERTY_DETAIL/OFFLINE/TXT_REMARK'),remark)
			println(remark)
			}
			else if (paytocode!= ''){
			WebUI.setText(findTestObject('Object Repository/CLAIM CHECKING/CLAIM_PROPERTY_DETAIL/OFFLINE/TXT_REMARK_tertanggung'), remark)
			println(remark)
			}
		}
		
	}else if (approval == 'Reject'){
	
		WebUI.click(findTestObject('Object Repository/CLAIM CHECKING/CLAIM_PROPERTY_DETAIL/OFFLINE/LST_APPROVAL - listapprovaloffline',[('listapprovaloffline'): approval]))
		
		WebUI.delay(3)
		//di kondisi karena path yang offline beda dengan online
		if (isonline == '1') {
			//variable di screen, di tulis di scenario
			WebUI.setText(findTestObject('Object Repository/CLAIM CHECKING/CLAIM_PROPERTY_DETAIL/ONLINE/TXT_REMARK'),remark)
			println(remark)
		}else if (isonline == '0' ){
		
			WebUI.setText(findTestObject('Object Repository/CLAIM CHECKING/CLAIM_PROPERTY_DETAIL/OFFLINE/TXT_REMARK'),remark)
			println(remark)
		}

	}
	
//WebUI.setText(findTestObject('Object Repository/CLAIM CHECKING/CLAIM_PROPERTY_DETAIL/OFFLINE/TXT_REMARK'),'Claim Checking OFFLINE')
	
WebUI.click(findTestObject('Object Repository/CLAIM CHECKING/CLAIM_PROPERTY_DETAIL/OFFLINE/BTN_SAVE'))
	
/*WebUI.verifyElementText(findTestObject('Object Repository/CLAIM CHECKING/CLAIM_PROPERTY_DETAIL/POPUP_SUCCESSFULLY'), 'Data has been successfully saved')

println("data berhasil di save")

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/CLAIM CHECKING/CLAIM_PROPERTY_DETAIL/BTN_NOTIFIKASI_X'))*/

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Tasklist/Button_X'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Logout/DRP_NAMA'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Logout/BTN_EXIT'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Web/Logout/BTN_YES'))

WebUI.delay(3)

WebUI.closeBrowser()