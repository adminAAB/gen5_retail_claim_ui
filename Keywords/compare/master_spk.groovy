package compare

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class master_spk {

	@Keyword
	def bandingkan () {
		def mspk = WebUI.getText(findTestObject('Object Repository/Web/Claim/List SPK')).trim()
		println(mspk)
		def masterspkDB = findTestData("SPK status").getValue(11, 1)
		println(masterspkDB)
		def result = WebUI.verifyMatch(mspk, masterspkDB, false)
		println(result)
		println ('master SPK :' + mspk + ' spkno ' + masterspkDB)
		if (result == true){
			WebUI.click(findTestObject('Object Repository/Web/Claim/Claim Checker/LST_SPK'))
			println('master SPKnya '+result +' nih')
		}
		else if (result == false){
			println('master SPKnya '+result +' nih')
		}
	}
}
