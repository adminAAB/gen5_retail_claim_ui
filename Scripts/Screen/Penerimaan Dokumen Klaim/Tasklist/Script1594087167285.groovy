import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI

//Proses search SPK dihalaman Penerimaan dokumen klaim

//Menentukan SPK pay to bengkel offline atau online
String SPKonline = UI.getValueDatabase("172.16.94.74", "AAB", "Select * from dbo.claim where claim_no='"+claimno+"'", "Is_Online_Workshop")

println (SPKonline)

//menentukan SPK partial loss atau total loss
String claimtype = UI.getValueDatabase("172.16.94.74", "AAB", "Select * from dbo.claim where claim_no='"+claimno+"'", "Type_Loss_Id")

println (claimtype)

//cek status SPK (digunakan untuk total loss)
String status = UI.getValueDatabase("172.16.94.74", "AAB", "SELECT * FROM claim_reception cr INNER JOIN dbo.Claim_Reception_SPK crs on crs.Reception_ID = cr.Reception_ID where SPK_No='"+claimno+"' and cr.rejectF='0'", 
    "Status")

//cek payto tertanggung
String payto = UI.getValueDatabase("172.16.94.74", "AAB", "Select * from dbo.claim where claim_no='"+claimno+"'", "workshop_code")
println(payto) 

//buat arraylist detail spk pay to bengkel
ArrayList detail1 = UI.getOneRowDatabase("172.16.94.74", "AAB", "SELECT '1' as NO, mw.Workshop_Name, Master_No, c.claim_no, CASE WHEN c.isb2b=0 THEN 'No' when c.isb2b=1 then 'Yes' END isB2B  FROM claim c INNER JOIN dbo.Mapping_Master_Claim mmc ON mmc.Claim_No = c.Claim_No INNER JOIN  cps..mst_workshop mw on c.workshop_code=mw.workshop_id where c.claim_no='"+claimno+"'")
println (detail1)

//buat arraylist detail spk pay to tertanggung
ArrayList detail2 = UI.getOneRowDatabase("172.16.94.74", "AAB", "SELECT '1' as NO, mc.name, Master_No, c.claim_no, CASE WHEN c.isb2b=0 THEN 'No' when c.isb2b=1 then 'Yes' END isB2B  FROM claim c INNER JOIN dbo.Mapping_Master_Claim mmc ON mmc.Claim_No = c.Claim_No INNER JOIN  mst_customer mc on c.pay_to_code=mc.cust_id where c.claim_no='"+claimno+"'")
println (detail2)

GEN5.ProcessingCommand()

/*if (((claimtype =='TLOSTL' )||(claimtype == 'TLOACC'))&& status =='15'){
	
	WebUI.click(null, FailureHandling.STOP_ON_FAILURE)

}*/

WebUI.setText(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Tasklist/TXT_SpkNo'),claimno)

WebUI.click(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Tasklist/Button_Search'))

WebUI.delay (5)

WebUI.click(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Tasklist/Top 1 Grid'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Tasklist/Button_Lanjut'))

GEN5.ProcessingCommand()

	if ((claimtype == 'TLOSTL') || (claimtype == 'TLOACC')||(claimtype =='PARACC' && payto =='')) {
	
		GEN5.CompareRowsValue(findTestObject('Penerimaan Dokumen Klaim/Penerimaan Dokumen/table_detail'), "No", "1", detail2)
		
	} else {
	
	GEN5.CompareRowsValue(findTestObject('Penerimaan Dokumen Klaim/Penerimaan Dokumen/table_detail'), "No", "1", detail1)
	}


WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Penerimaan Dokumen/select_data',[('claimno') : claimno]))

WebUI.delay(2)

//WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Penerimaan Dokumen/checklist_data'))
GEN5.tickExpectedCheckbox(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Tasklist/Tbl_grid'), 'No. SPK', claimno)

WebUI.delay(2)

WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Penerimaan Dokumen/button_Lanjut'))

WebUI.delay(2)

WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Penerimaan Dokumen/dropdown_doctype'))

WebUI.delay(2)
	if ((SPKonline == '1') || (SPKonline == '0' && status == '15')) {
		WebUI.click(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Penerimaan Dokumen/choose_doctype', [('doctype') : 'Kuitansi']))

	} else if (SPKonline == '0' && payto != '' &&((claimtype == 'PARACC') || (claimtype == 'PARSTO')))  {
  		WebUI.click(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Penerimaan Dokumen/choose_doctype', [('doctype') : 'Estimasi']))
	
	} else if ((status == '19') && ((claimtype == 'TLOACC') || (claimtype == 'TLOSTL'))) {
    	WebUI.click(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Penerimaan Dokumen/choose_doctype', [('doctype') : 'Akhir']))

	} else if ((claimtype == 'TLOACC') || (claimtype == 'TLOSTL')) {
		WebUI.click(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Penerimaan Dokumen/choose_doctype', [('doctype') : 'Awal']))
		
	} else if (payto == '' && SPKonline =='0' && ((claimtype == 'PARACC') || (claimtype == 'PARSTO'))){
		WebUI.click(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Penerimaan Dokumen/choose_doctype', [('doctype') : 'Kuitansi']))
	}
WebUI.click(findTestObject('Penerimaan Dokumen Klaim/Penerimaan Dokumen/button_Lanjut(doctype)'))

GEN5.ProcessingCommand()

//kondisi jika terdapat informasi dokumen pernah diterima sebelumnya

	if (status == '1' || status == '2' ) {
		
		WebUI.click(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Penerimaan Dokumen/button_information(terimaulang)',[('answer') : answer] ))
	} 




