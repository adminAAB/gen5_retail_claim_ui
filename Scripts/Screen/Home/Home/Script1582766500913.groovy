import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5
import com.keyword.UI

//Proses pilih menu dan submenu

GEN5.ProcessingCommand()

//login EDN akan membuka menu Penerimaan Dokumen Klaim
if (proses =='dokumen') {
		
	WebUI.click(findTestObject('Home/button_menu',[('menu') : 'Claim']))
	
	WebUI.click(findTestObject('Home/button_submenu',[('menu') : 'Claim', ('submenu') : 'Penerimaan Dokumen']))
	
//selain EDN (OMA, SST) akan membuka menu Claim Checking
} else {

	WebUI.click(findTestObject('Home/button_menu',[('menu') : 'Claim']))
	
	WebUI.click(findTestObject('Home/button_submenu',[('menu') : 'Claim', ('submenu') : 'Checking']))
}
