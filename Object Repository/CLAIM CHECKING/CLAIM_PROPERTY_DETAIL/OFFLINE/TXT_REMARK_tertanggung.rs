<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TXT_REMARK_tertanggung</name>
   <tag></tag>
   <elementGuidId>935ad8aa-4272-4518-8458-c4186eeb6326</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[not(contains(@style,&quot;none&quot;)) and @data-bind=&quot;visible: ($parent.active()==$index())&quot; and @style]//div[contains(@id,&quot;pageset&quot;)]//div[contains(@class,&quot;a2is-content-for-pageset&quot;)]//h3[text()=&quot;Claim Property Detail&quot;]/parent::*//textarea[@placeholder='Remarks']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[not(contains(@style,&quot;none&quot;)) and @data-bind=&quot;visible: ($parent.active()==$index())&quot; and @style]//div[contains(@id,&quot;pageset&quot;)]//div[contains(@class,&quot;a2is-content-for-pageset&quot;)]//h3[text()=&quot;Claim Property Detail&quot;]/parent::*//textarea[@placeholder='Remarks']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/FRAME</value>
   </webElementProperties>
</WebElementEntity>
