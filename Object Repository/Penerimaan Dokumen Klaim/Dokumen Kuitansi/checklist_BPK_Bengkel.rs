<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>checklist_BPK_Bengkel</name>
   <tag></tag>
   <elementGuidId>0297c9db-a3ba-4fa9-82dd-90f783f668f8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;CDRP-0&quot;]//span[text()='BPK Bengkel']/parent::label/following-sibling::div//span[text()='Received']/parent::div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;CDRP-0&quot;]//span[text()='BPK Bengkel']/parent::label/following-sibling::div//span[text()='Received']/parent::div/input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/FRAME</value>
   </webElementProperties>
</WebElementEntity>
