import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Claim Tracking/expand_claim'))

println('sudah masuk ke halaman claim tracking')

WebUI.click(findTestObject('Object Repository/Claim Tracking/btn_claim tracking'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Claim Tracking/btn_date picker'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Claim Tracking/btn_date picker'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Claim Tracking/btn_drop down'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Claim Tracking/btn_waiting estimation approval'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Claim Tracking/btn_search'))

GEN5.ProcessingCommand()

String count_page = WebUI.getText(findTestObject('Object Repository/Claim Tracking/page count'))

//String count_page 

String[] halaman_awal = count_page.split(" ")

for (i=1;i<halaman_awal[2].toInteger();i++){
	WebUI.click(findTestObject('Object Repository/Claim Tracking/btn_forward'))
	GEN5.ProcessingCommand()
}




