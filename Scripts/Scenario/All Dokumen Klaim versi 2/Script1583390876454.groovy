import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI

//cari data claimno
String offline = UI.getValueDatabase("172.16.94.74", "AAB", "SELECT TOP 1 * FROM claim c WHERE workshop_code='00453417' and user_id='BTE' and not EXISTS (SELECT * FROM  claim_reception_spk crs WHERE c.Claim_No=crs.SPK_No)  AND claimable_status <>'2'", 
    'claim_no')

println(offline)

String online = UI.getValueDatabase("172.16.94.74", "AAB", "SELECT TOP 1 * FROM claim c INNER JOIN cps..trx_spk ts on ts.spk_no=c.Claim_No INNER JOIN dbo.Claim_Reception_SPK crs on ts.SPK_No=crs.spk_no INNER JOIN dbo.Claim_Reception cr on cr.Reception_ID = crs.Reception_ID WHERE c.workshop_code='90332449'   AND ts.SPK_Status='05'  and cr.Reception_ID like '%PLCE%' and cr.RejectF=0 and c.claimable_status <>'2'", 
    "claim_no")

println(online)

String AwalTLS = UI.getValueDatabase("172.16.94.74", "AAB", "select top 1 * from claim c where type_loss_id='TLOSTL' and USER_id='fmw' and workshop_code='' and not EXISTS (SELECT * FROM  claim_reception_spk crs WHERE c.Claim_No=crs.SPK_No)  AND claimable_status <>'2' order by entrydt desc", 
    "claim_no")

println(AwalTLS)

String AwalTLA = UI.getValueDatabase("172.16.94.74", "AAB", "select top 1 * from claim c where type_loss_id='TLOACC' and USER_id='fmw'  and workshop_code='' and not EXISTS (SELECT * FROM  claim_reception_spk crs WHERE c.Claim_No=crs.SPK_No)  AND claimable_status <>'2' order by entrydt desc", "claim_no")

println(AwalTLA)

String Akhir = UI.getValueDatabase("172.16.94.74", "AAB", "SELECT top 1 * FROM claim c inner join claim_reception_spk crs ON c.claim_no=crs.spk_no inner join claim_reception cr  on crs.reception_id = cr.Reception_ID where  type_loss_id in ('TLOSTL','TLOACC') AND workshop_code='' and cr.status='19' and cr.rejectF='0' AND claimable_status <>'2'", 
    "claim_no")

println(Akhir)

//define claimno
ArrayList claimno = [offline, online, AwalTLS, AwalTLA, Akhir]

int i

//define semua variable yang ada di screen
String userid = 'EDN'

String password = 'P@ssw0rd'

String product = 'Konvensional'

String nomorrangka = UI.getValueDatabase("172.16.94.74", "AAB", "SELECT top 1 * FROM claim c INNER JOIN dtl_mv dm ON c.policy_id=dm.policy_id where c.claim_no='"+claimno[1]+"'", "chasis_number")

String nilaiestimasi = '3000000'

String remarks = 'remarks ambar'

String receivedate = '04/Mar/2020'

String invoicedate = '04/Mar/2020'

String nomorkuitansi = '1234567890'

String nilaikuitansi = '3000000'

String SAname = 'Farida Ambarwati'

String nomorBPKB = '1234567890'

WebUI.openBrowser('https://gen5-qc.asuransiastra.com/retail')

WebUI.maximizeWindow()

WebUI.callTestCase(findTestCase('Screen/Login/Login'), [('userid') : userid, ('password') : password, ('product') : product],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Screen/Home/Home'), [('userid') : userid], FailureHandling.STOP_ON_FAILURE)

for (i = 0; i < claimno.size(); i++) {

    WebUI.callTestCase(findTestCase('Screen/Penerimaan Dokumen Klaim/Penerimaan Dokumen'), [('claimno') : claimno[i]], FailureHandling.STOP_ON_FAILURE)
	
	WebUI.callTestCase(findTestCase('Screen/Penerimaan Dokumen Klaim/All dokumen'), 
		[('claimno') : claimno[i],
			('nomorrangka') : nomorrangka,
			('nilaiestimasi') : nilaiestimasi,
			('nilaikuitansi') : nilaikuitansi,
			('receivedate') : receivedate,
			('invoicedate') : invoicedate,
			('SAname') : SAname,
			('nomorBPKB') : nomorBPKB,
			('remarks') : remarks ], FailureHandling.STOP_ON_FAILURE)
}



