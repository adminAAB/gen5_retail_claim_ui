import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testdata.DBData

import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

import com.keyword.UI
import com.keyword.GEN5

GEN5.ProcessingCommand()

//status di claim reception
def spkstatus = findTestData("SPK status").getValue(10, 1)
def spkno = findTestData("SPK status").getValue(2, 1)
def masterspk = findTestData("SPK status").getValue(11, 1).trim()

//dipakai buat dapetin claim typenya
String claimtype = UI.getValueDatabase("172.16.94.74", "AAB", "Select * from dbo.claim where claim_no='"+claimno+"'", "Type_Loss_Id")

println (claimtype)
//status SPK online atau offline
def isonline = UI.getValueDatabase("172.16.94.74", "AAB", "Select * from dbo.claim where claim_no='"+claimno+"'", "Is_Online_Workshop")

//dipakai untuk kondisi validasi pay to ke tertanggung atau ke bengkel online/offline
def paytocode =  UI.getValueDatabase("172.16.94.74", "AAB", "Select * from dbo.claim where claim_no='"+claimno+"'", "pay_to_code")

//dipakai untuk lihat status dokumen akhir
//def status = UI.getValueDatabase("172.16.94.74", "AAB", "SELECT top 1 * FROM claim c inner join claim_reception_spk crs ON c.claim_no=crs.spk_no inner join claim_reception cr on crs.reception_id = cr.Reception_ID where type_loss_id in ('TLOSTL','TLOACC') AND cr.Status='19' AND workshop_code='' and cr.rejectF='0' AND claimable_status <>'2'order by c.entrydt DESC","status")
def status = UI.getValueDatabase("172.16.94.74", "AAB", "SELECT top 1 * FROM claim c inner join claim_reception_spk crs ON c.claim_no=crs.spk_no inner join claim_reception cr on crs.reception_id = cr.Reception_ID where claim_no='"+claimno+"'","status")
println (isonline)
println(spkstatus)
//println(spkno)
println(paytocode)
println(status)

//tambahan : checker estimasi online, spk belum terinsert ke claim reception, jadi menggunakan status proses bengkel di trx_spk harus 03
String statusbengkel =  UI.getValueDatabase("172.16.94.74", "AAB", "SELECT TOP 1 * FROM claim c INNER JOIN cps..trx_spk ts on ts.spk_no=c.Claim_No WHERE c.claim_no='"+claimno+"'",
	"spk_status")
println(statusbengkel)

WebUI.delay(3)

if(spkstatus == '2' || spkstatus =='1' || statusbengkel =='03' ) {
	
	WebUI.setText(findTestObject('Object Repository/Web/Claim/Claim Checker/TXT_SPKNO'), claimno) //ambil claimno diskenario
	
	println(claimno)
	
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Claim/Claim Checker/BTN_SEARCH'))
	
	WebUI.delay(3)
	
/*	//CustomKeywords.'compare.master_spk.bandingkan'()
	if (statusbengkel =='03') {
		
		}  else {
		
		DBData validasi = findTestData("validasi spk")
	validasi.query = validasi.query.replace('_spkno_',claimno)
	validasi.fetchedData = validasi.fetchData()
	
	def mspk = UI.getValueDatabase("172.16.94.74", "AAB", "Select top 1 * from dbo.claim c inner join mapping_master_claim mmc on c.claim_no=mmc.claim_no where c.claim_no='"+claimno+"'", "master_no")
	def aging = validasi.getValue(2, 1)
	def description =  validasi.getValue(3, 1)
	def receipt_date = validasi.getValue(4, 1)
	def invoice_date = validasi.getValue(5, 1)
	def workshop_name = validasi.getValue(6, 1)
	
	ArrayList compare = [mspk,aging,description,receipt_date,invoice_date,workshop_name] 
	GEN5.CompareRowsValue(findTestObject('Object Repository/Web/Claim/Claim Checker/KOLOM_HEADER'), 'No', '1', compare)
	println(compare)
	
	}*/
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Claim/Claim Checker/LST_SPK'))
	
	WebUI.delay(2)
	
	WebUI.click(findTestObject('Object Repository/Web/Claim/Claim Checker/BTN_NEXT'))
	
	GEN5.ProcessingCommand()
		
	WebUI.click(findTestObject('Object Repository/Web/Claim/Claim Checker/BTN_CHECK_TAB_SPK'))
	
	WebUI.delay(5)
	
	WebUI.click(findTestObject('Object Repository/Web/Claim/Claim Checker/BTN_TAB_DOC'))
	
	WebUI.delay(5)
	
		if(paytocode == 'null' || paytocode == ''){
			
		println(paytocode)
			
			WebUI.delay(2)
			
			WebUI.click(findTestObject('Object Repository/Web/Claim/Claim Checker/BTN_CHECK_TAB_DOC_bengkel'))
			
		}else if (paytocode != ''){
			
			WebUI.delay(2)
			
			WebUI.click(findTestObject('Object Repository/Web/Claim/Claim Checker/BTN_CHECK_TAB_DOC_tertanggung'))
			
			}
	WebUI.delay(2)
	
	WebUI.click(findTestObject('Object Repository/Web/Claim/Claim Checker/BTN_TAB_CLAIM_INFO'))
	
	WebUI.delay(2)
	
	WebUI.click(findTestObject('Object Repository/Web/Claim/Claim Checker/BTN_CHECK_TAB_CLAIM_INFO'))
	
	WebUI.delay(3)
	
	WebUI.click(findTestObject('Object Repository/Web/Claim/Claim Checker/BTN_TAB_PHOTO'))
	
	GEN5.ProcessingCommand()
	
	WebUI.click(findTestObject('Object Repository/Web/Claim/Claim Checker/BTN_CHECK_TAB_PHOTO'))
	
	WebUI.delay(1)
	
	WebUI.click(findTestObject('Object Repository/Web/Claim/Claim Checker/BTN_NEXT_2'))
	
	GEN5.ProcessingCommand()
	
if (isonline == '1' && statusbengkel =='03' ) { //kondisi checklist aktif (estimasi SPKonline)
	
	GEN5.ProcessingCommand()
		
	/*GEN5.tickAllCheckboxInTable(findTestObject('Object Repository/Web/Claim/Claim Checker/TABLE_PANEL_ONLINE'))
		*/
	WebUI.scrollToElement(findTestObject('Object Repository/Web/Claim/Claim Checker/TXT_SCROLL_TABLE'), 1, FailureHandling.STOP_ON_FAILURE)
		
	GEN5.tickAllCheckboxInTable(findTestObject('Object Repository/Web/Claim/Claim Checker/TABLE_PANEL_ONLINE'))
		
	WebUI.setText(findTestObject('Web/Claim/Penerimaan Doc Claim/Doc Claim Estimasi/TXT_REMARK_ONLINE'), 'claim checker automate '+claimno)

	
}else if (isonline == '1' ) {

	WebUI.delay(3)
		
	WebUI.setText(findTestObject('Web/Claim/Penerimaan Doc Claim/Doc Claim Estimasi/TXT_REMARK_ONLINE'), 'claim checker automate online '+claimno)
			
} else if (isonline == '0') {

	WebUI.delay(3)
			
	WebUI.setText(findTestObject('Web/Claim/Penerimaan Doc Claim/Doc Claim Estimasi/TXT_REMARK_OFFLINE'), 'claim checker automate '+claimno)

} 

if (status == '2' && claimtype == 'TLOACC'){
	println(status)
	
	GEN5.ProcessingCommand()
			
		WebUI.delay(3)
		
		WebUI.click(findTestObject('Object Repository/Web/Claim/Claim Checker/DATE_PICK_Mgr_Surveyor'))
		
		WebUI.delay(3)
		
		//GEN5.DatePicker(sendmgr, findTestObject('Object Repository/Web/Claim/Claim Checker/CHOOSE_DATE_MGR'))
		
		WebUI.delay(3)
		
		WebUI.click(findTestObject('Object Repository/Web/Claim/Claim Checker/DATE_PICK_from_Mgr'))
		
		WebUI.delay(3)
		
		//GEN5.DatePicker(frommgr, findTestObject('Object Repository/Web/Claim/Claim Checker/CHOOSE_DATE_FROM_MGR'))
		
		WebUI.delay(3)
		
		WebUI.click(findTestObject('Object Repository/Web/Claim/Claim Checker/BTN_PRINT_PRD'))
		
		WebUI.delay(3)
		
		/*WebUI.click(findTestObject('Object Repository/Web/Claim/Claim Checker/BTN_X'))
		
		WebUI.delay(3)*/
			
	} else if (status == '2' && claimtype == 'TLOSTL'){
		
		WebUI.click(findTestObject('Object Repository/Web/Claim/Claim Checker/BTN_PRINT_PRD'))
			
		WebUI.delay(3)
			
		/*WebUI.click(findTestObject('Object Repository/Web/Claim/Claim Checker/BTN_X'))
			
		WebUI.delay(3)*/
	
	}
		
	WebUI.delay(3)

	if(paytocode=='null' || paytocode == ''){
	
		WebUI.click(findTestObject('Object Repository/Web/Claim/Claim Checker/EXP_APPROVAL_bengkel'))
	
		}else if (paytocode!='') {
		
			WebUI.click(findTestObject('Object Repository/Web/Claim/Claim Checker/EXP_APPROVAL_tertanggung'))
		
		}
	
WebUI.click(findTestObject('Object Repository/Web/Claim/Claim Checker/LST_APPROVAL',[('Approval') : Approval]))

WebUI.delay(3)
	
WebUI.click(findTestObject('Object Repository/Web/Claim/Claim Checker/BTN_SAVE'))
	
WebUI.delay(1)

}

GEN5.ProcessingCommand()

//WebUI.click(findTestObject('Object Repository/Penerimaan Dokumen Klaim/Tasklist/Button_X'))


/*WebUI.click(findTestObject('Object Repository/Web/Logout/DRP_NAMA'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Web/Logout/BTN_EXIT'))

WebUI.delay(3)*/