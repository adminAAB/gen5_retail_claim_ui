<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TBL_DATA_SEARCH</name>
   <tag></tag>
   <elementGuidId>f2610226-8a30-4664-8178-e9dff00d57ce</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;pageset1-0&quot;]//table[@class='table table-striped table-condensed a2is-datatable']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;pageset1-0&quot;]//table[@class='table table-striped table-condensed a2is-datatable']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/FRAME</value>
   </webElementProperties>
</WebElementEntity>
