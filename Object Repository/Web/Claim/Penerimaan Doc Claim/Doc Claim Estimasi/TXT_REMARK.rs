<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TXT_REMARK</name>
   <tag></tag>
   <elementGuidId>d90c0d1c-ef10-4302-8507-cbda19c20253</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[not(contains(@style,&quot;none&quot;)) and @data-bind=&quot;visible: ($parent.active()==$index())&quot; and @style]//div[contains(@id,&quot;pageset&quot;)]//div[contains(@class,&quot;a2is-content-for-pageset&quot;)]//h3[text()=&quot;Claim Property Detail&quot;]/parent::*//*[@id=&quot;P2Ta0-0&quot;]/div[1]/div[4]/div[2]/a2is-textarea-wide-nc/div[2]/div[1]/div/textarea[count(. | //*[@ref_element = 'Object Repository/FRAME']) = count(//*[@ref_element = 'Object Repository/FRAME'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[not(contains(@style,&quot;none&quot;)) and @data-bind=&quot;visible: ($parent.active()==$index())&quot; and @style]//div[contains(@id,&quot;pageset&quot;)]//div[contains(@class,&quot;a2is-content-for-pageset&quot;)]//h3[text()=&quot;Claim Property Detail&quot;]/parent::*//*[@id=&quot;P2Ta0-0&quot;]/div[1]/div[4]/div[2]/a2is-textarea-wide-nc/div[2]/div[1]/div/textarea</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/FRAME</value>
   </webElementProperties>
</WebElementEntity>
