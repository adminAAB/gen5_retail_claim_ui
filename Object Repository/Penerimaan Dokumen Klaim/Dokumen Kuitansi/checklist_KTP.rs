<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>checklist_KTP</name>
   <tag></tag>
   <elementGuidId>de089e88-54e6-43e7-b41d-56e0885c0552</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;CDRP-0&quot;]//span[text()='KTP &amp; No. Telepon']/parent::label/following-sibling::div//span[text()='Received']/parent::div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;CDRP-0&quot;]//span[text()='KTP &amp; No. Telepon']/parent::label/following-sibling::div//span[text()='Received']/parent::div/input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/FRAME</value>
   </webElementProperties>
</WebElementEntity>
