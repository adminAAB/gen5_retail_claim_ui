import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI

//cari data claimno SPK tertanggung dan belum pernah dilakukan terima dokumen kuitansi di GEN5
String claimno = '3200001325' //UI.getValueDatabase("172.16.94.74", "AAB", "SELECT Top 1 * FROM AAB..Claim  AS c INNER JOIN aab..Mapping_Master_Claim mmc on c.Claim_No=mmc.Claim_No INNER JOIN aab..Claim_Reception cr on mmc.Master_No=cr.Master_No INNER JOIN aab..Invoice_Note ino on c.policy_no=ino.Policy_No AND claimable_Status <>'2' AND cr.RejectF='0' AND Type_Loss_Id = 'TLOSTL' AND Is_Online_Workshop='0' AND workshop_code='' and ino.Settle_Status='T' and ino.Note_Type='DPR' and cr.Status='19' AND Claim_GroupID<>'' ORDER BY c.EntryDt DESC",
	//"claim_no")

println(claimno)


//define semua variable yang diperlukan di screen
// proses dokumen awal : penerimaan dokumen, checker, approval
//arraylist berisi proses yang akan dilakukan, ini akan digunakan di screen home
ArrayList proses = ['dokumen','checking']
int i

String password = 'ITG@nt1P455QC'

String tipelogin = 'a2is Retail - Konvensional'

String nomorrangka = UI.getValueDatabase("172.16.94.74", "AAB", "SELECT top 1 * FROM claim c INNER JOIN dtl_mv dm ON c.policy_id=dm.policy_id where c.claim_no='"+claimno+"'", "chasis_number")

String nilaiestimasi = '3000000'

String remark = 'remarks ambar'

String receivedate = '09/Mar/2020'

String nomorkuitansi = '1234567890'

String SAname = 'Farida Ambarwati'

String nomorBPKB = '1234567890'



for (i = 0; i < proses.size(); i++) {
	
	WebUI.openBrowser('https://gen5-qc.asuransiastra.com/retail')
	
	WebUI.maximizeWindow()
	
	WebUI.callTestCase(findTestCase('Screen/Login/Login'), 
		[('password') : password,
		('tipelogin') : tipelogin , 
		('claimno') : claimno,
		('proses') : proses[i] ],
		FailureHandling.STOP_ON_FAILURE)
	
	WebUI.callTestCase(findTestCase('Screen/Home/Home'), [('proses') : proses[i]], FailureHandling.STOP_ON_FAILURE)
	
	if (proses[i] == 'dokumen') {
		
		WebUI.callTestCase(findTestCase('Screen/Penerimaan Dokumen Klaim/Tasklist'),
			[('claimno') : claimno,
			('answer') : 'Yes'],
		 FailureHandling.STOP_ON_FAILURE)
		
		//WebUI.callTestCase(findTestCase('Screen/Penerimaan Dokumen Klaim/Penerimaan Dokumen'), [('claimno') : claimno], FailureHandling.STOP_ON_FAILURE)
		
			WebUI.callTestCase(findTestCase('Screen/Penerimaan Dokumen Klaim/All dokumen'),
			[('claimno') : claimno,
				('nomorrangka') : nomorrangka,
				('nilaiestimasi') : nilaiestimasi,
				//('nilaikuitansi') : nilaikuitansi,
				('receivedate') : receivedate,
				//('invoicedate') : invoicedate,
				('SAname') : SAname,
				('nomorBPKB') : nomorBPKB,
				('remarks') : remark ], FailureHandling.STOP_ON_FAILURE)
			
		
	} else if (proses[i] == 'checking') {
	
	WebUI.callTestCase(findTestCase('Screen/claim/Claim Checking'),  [('claimno') : claimno , ('Approval') : 'Approve'], FailureHandling.STOP_ON_FAILURE)
	
	} else if (proses[i] == 'approval') {
		
		WebUI.callTestCase(findTestCase('Screen/Claim Checking/Claim Checking Detail'), [('claimno') : claimno , ('remark') : 'Approval berhasil cco '+claimno, ('approval') : 'Approve'],   FailureHandling.STOP_ON_FAILURE)
	}
	
}